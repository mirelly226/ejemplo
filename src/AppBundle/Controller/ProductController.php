<?php

namespace AppBundle\Controller;

use AppBundle\Dto\ProductDto;
use AppBundle\Entity\Product;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Rest\Rout("api/v1/product")
 */
class ProductController extends FOSRestController
{
    /**
     * @Rest\Get("")
     */
    public function getAllProducts()
    {
        $productsBD = $this->getDoctine()->getRepository(Product::class)->findAll();
        $products = array();
        foreach($productsBD as $productsBD){
            $product= new ProductDto($productsBD);
            array_push($products, $product);
        }
        return $products;
    }

}